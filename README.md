# ACB-Nebraska Theme

This repo contains the Drupal 8 theme files and accessibility fixes that we built for American Council of the Blind of Nebraska. 
This project was part of the submission for Knowbility's OpenAIR 2018 Web Accessibility competition that won first place and had the best score in 20 years of the competition's existence. Our approach to this project was to make it accessible from the ground up. This include ensuring proper semantic HTML is used as a solid foundation before building on features with JavaScript and Drupal functionalities. The theme and site meets all WCAG 2.0 success criterias with provisions for WCAG 2.1. 
## Contributions
My contribution included translating user requirements into mockups, wireframes, prototypes using Adobe XD, subsequently building out the Information Architecture, creating the theme, layout, content strategy, and finally QA testing. See screenshots at [my portfolio site](https://www.iamrichardlock.com/portfolio).