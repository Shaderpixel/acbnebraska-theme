'use strict';

(function() {
    function Disclosure(container, controls, isOpenClass, applyIsOpenToContainer) {
        this.container = container;
        this.isOpenClass = isOpenClass;
        this.applyIsOpenToContainer = applyIsOpenToContainer;
        this.button = this.container.querySelector('button');
        this.body = document.querySelector('body');
        this.isMobile = false;
        this.lastInteraction = new Date().getTime();

        //Get the control
        let id = this.button.getAttribute('aria-controls');
        if (controls) {
            this.controls = controls;
        } else if (id) {
            this.controls = document.getElementById(id);
        } else {
            //Assume that it is controlling the next sibling element
            this.controls = this.button.nextElementSibling;
        }

        //Determine which element should receive focus
        this.focusTarget = this.controls.querySelector('h2, a, button, input, textarea');
        this.focusTarget.setAttribute('tabindex', '0'); //Ensure that the element is focusable.

        //Detect and alter active/deactivated states on width change
        const mq = window.matchMedia("(min-width: 41.95625em)");
        mq.addListener(function() {
            this.onWidthChange(mq);
        }.bind(this));
        this.onWidthChange(mq);

        //Toggle
        this.button.addEventListener('click', function (e) {
            //update the last interaction time
            if ((new Date().getTime() - this.lastInteraction) < 400) {
                //The last interaction happened 10ms ago.
                //changes are the user just clicked on the button. Try not to close it unexpectedly
                //example: user hovers over button and clicks right away to open the menu
                return;
            }

            this.lastInteraction = new Date().getTime();
            this.toggle();
        }.bind(this));

        //Handle the escape action
        this.container.addEventListener('keyup', function (e) {
            // If escape, refocus menu button
            if (e.keyCode === 27) {
                //update the last interaction time
                this.lastInteraction = new Date().getTime();
                this.close();
            }
        }.bind(this));

        hoverintent(this.container, function() {
            // Handler in
            if (this.isMobile) {
                return;
            }

            if (this.buttonIsHidden()) {
                return;
            }

            if ((new Date().getTime() - this.lastInteraction) < 900) {
                //The last interaction happened 10ms ago.
                //changes are the user just clicked on the button. Try not to close it unexpectedly
                //example: user hovers over button and clicks right away to open the menu
                return;
            }

            //update the last interaction time
            this.lastInteraction = new Date().getTime();

            this.open(true);
        }.bind(this), function() {
            //handler out
            if (this.isMobile) {
                return;
            }

            if (this.buttonIsHidden()) {
                return;
            }

            this.close();
        }.bind(this));

        //Close when something outside of the the container is clicked
        this.body.addEventListener('click', function(e) {
            if (!this.isExpanded()) {
                return;
            }

            if (this.container.contains(e.target)) {
                //a child of the menu was clicked, we don't need to close it
                return;
            }

            this.close();
        }.bind(this));

        //Close when something else receives focus
        this.body.addEventListener('focusin', function(e) {
            if (!this.isExpanded()) {
                return;
            }

            if (this.container.contains(e.target)) {
                //a child of the menu was clicked, we don't need to close it
                return;
            }

            this.close();
        }.bind(this));
    }

    Disclosure.prototype.open = function (doNotFocusChild) {
        if (this.isExpanded()) {
            //We are already open...
            return false;
        }
        
        this.controls.removeAttribute('hidden');

        //Hack to trigger css transition
        setTimeout(function(){
            this.button.setAttribute('aria-expanded', 'true');
            if (this.applyIsOpenToContainer) {
                this.container.classList.add(this.isOpenClass);
            } else {
                this.body.classList.add(this.isOpenClass);
            }

            //Send focus to the first item
            if (!doNotFocusChild) {
                this.focusTarget.focus();
            }
        }.bind(this), 5);
    };

    Disclosure.prototype.close = function () {
        if (!this.isExpanded()) {
            //We are already closed...
            return false;
        }
        
        if (this.applyIsOpenToContainer) {
            this.container.classList.remove(this.isOpenClass);
        } else {
            this.body.classList.remove(this.isOpenClass);
        }

        //Hack to trigger css transition
        setTimeout(function(){
            this.button.setAttribute('aria-expanded', 'false');
            this.controls.setAttribute('hidden', true);

            //Send focus back to the button if a child of the menu is currently selected
            if (document.activeElement && this.controls.contains(document.activeElement)) {
                this.button.focus();
            }
        }.bind(this), 300);
    };

    Disclosure.prototype.toggle = function () {
        if (this.isExpanded()) {
            this.close();
        } else {
            this.open();
        }
    };

    Disclosure.prototype.isExpanded = function () {
        return this.button.getAttribute('aria-expanded') === 'true';
    };

    Disclosure.prototype.buttonIsHidden = function() {
        return (this.button.offsetParent === null);
    };

    /**
     * Activate this disclosure so that it is functional
     */
    Disclosure.prototype.activate = function() {
        this.button.setAttribute('aria-expanded', 'false');
        this.controls.setAttribute('hidden', true);
    };

    /**
     * Deactivate this disclosure so that it is not functional
     *
     * this will remove and disable our custom managed attributes, classes, etc
     */
    Disclosure.prototype.deactivate = function() {
        if (this.applyIsOpenToContainer) {
            this.container.classList.remove(this.isOpenClass);
        } else {
            this.body.classList.remove(this.isOpenClass);
        }
        this.controls.removeAttribute('hidden');
    };

    /**
     * This will toggle activate/deactivate
     */
    Disclosure.prototype.onWidthChange = function (mq) {
        if (mq.matches) {
            this.isMobile = false;
        } else {
            this.isMobile = true;
        }

        /**
         * Let outside CSS tell us when a disclosure should be considered deactivated
         * Base this decision on weather or not the button is visible
         */
        if (this.buttonIsHidden()) {
            this.deactivate();
        } else {
            this.activate();
        }
    };

    //wire up the mobile menu
    let mobileNavMenu = document.querySelector('.c-menu');
    mobileNavMenu = new Disclosure(mobileNavMenu, mobileNavMenu.querySelector('.c-menu-list'), 'js-nav--is-open');

    //wire up the mobile search
    let mobileSearch = document.querySelector('.c-menu + .c-search');
    mobileSearch = new Disclosure(mobileSearch, mobileSearch.querySelector('form'), 'js-search--is-open');

    //wire up desktop menu buttons
    let submenuDisclosures = [];
    let containers = document.querySelectorAll('.c-menu-list__item.has-child');
    for (let i = 0; i < containers.length; ++i) {
        let tmp = containers[i];
        tmp = new Disclosure(tmp, tmp.querySelector('ul'), 'js-submenu--is-open', true);
        submenuDisclosures.push(tmp);
    }


    /**
     * Dynamically sets the margin-top css of the element right after the <header> to the height of the <header> on mobile
     */
    function resizeHeader() {
        let header = document.querySelector('header');
        let afterHeader = header.nextElementSibling;
        let isMobile = false; //track if we are on mobile or not

        //Use a media query to help determine if we need to watch width change or not
        function onWidthChange(mq) {
            if (mq.matches) {
                //desktop
                isMobile = false;
                afterHeader.removeAttribute('style');
            } else {
                //mobile
                isMobile = true;
            }
        }

        //Watch for window resizing
        const mq = window.matchMedia("(min-width: 41.95625em)");
        mq.addListener(function () {
            onWidthChange(mq);
        });
        onWidthChange(mq);

        //performance helper
        function debounce(func, wait, immediate) {
            let timeout;
            return function () {
                let context = this, args = arguments;
                let later = function () {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                let callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        }

        let onResize = debounce(function () {
            if (!isMobile) {
                return;
            }

            let height = header.offsetHeight;
            afterHeader.setAttribute('style', 'margin-top: ' + height + 'px');
        }, 250);

        window.addEventListener('resize', onResize);

        //Make sure this always fires at least once
        onResize();
    }

    //Resize the header on mobile (margin-top)
    resizeHeader();

    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector ||
            Element.prototype.webkitMatchesSelector;
    }

    if (!Element.prototype.closest) {
        Element.prototype.closest = function (s) {
            var el = this;
            if (!document.documentElement.contains(el)) return null;
            do {
                if (el.matches(s)) return el;
                el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType === 1);
            return null;
        };
    }

    //override skip nav and in-page-links and force focus on target
    document.querySelector('body').addEventListener("click", function(e) {
        if (!e.target) {
            // we need a target
            return;
        }

        //element.closest() is supported in safari 9.0. Not supported in IE11, but we are already targeting safari.
        //if we use this on other browsers, we will likely need a polyfill for .closest()
        let link = e.target.closest('a');

        if (!link) {
            return;
        }

        if (!link.matches('a[href^="#"]')) {
            return;
        }

        let id = link.getAttribute('href').slice(1);
        let target = document.getElementById(id);

        if (!target) {
            return;
        }

        //Prevent the default actions
        e.preventDefault();

        if (target.matches('main, section, footer, header, span, div, h1, h2, h3, h4, h5, h6')) {
            //make sure the element can be programatically focused
            target.setAttribute('tabindex', '-1');
        }

        target.focus();
    });
})();


